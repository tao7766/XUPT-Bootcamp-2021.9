## Git安装

1. Linux

```bash
sudo apt install git
```

2. Windows

```bash
# 国内下载镜像
https://mirrors.tuna.tsinghua.edu.cn/github-release/git-for-windows/git/LatestRelease/
```

## Git配置

1. 配置用户名（代码的作者）

```bash
git config --global user.name "LIU Yu"
```

2. 配置Email（作者的联系方式）

```bash
git config --global user.email liuy_xa@hqyj.com
```

3. 配置文本编辑器（编写提交信息）

```bash
git config --global core.editor C:\Windows\System32\notepad.exe
```

4. 查看修改的配置

```bash
git config --list
```

## 保存修改

1. 创建工作目录和本地代码库

```bash
git init
```

2. 查看当前工作目录状态

```bash
git status
```

3. 将修改添加到暂存区

```bash
git add main.c CMakeLists.txt
```

4. 将修改提交到代码库

```bash
git commit
# 必须在弹出的文本编辑器中填写提交信息
```

## 比较差异

1. 查看提交历史

```bash
git log
```

2. 比较差异

```bash
git diff <修改前的提交ID> <修改后的提交ID>

#提交ID可以使用HEAD~数字代替
```

## 恢复代码

```bash
git checkout HEAD .
# HEAD表示恢复到代码库中的最新版本
# 句点代表所有文件
```

## 删除和重命名文件

```bash
git rm 文件名
git mv 旧文件名 新文件名
git commit
```

## 下载远程库

```bash
git clone https://gitee.com/hqyjxa/XUPT-Bootcamp-2021.9.git
```



